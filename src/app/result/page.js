'use client'
import {useSearchParams} from "next/navigation";

export default function PageResult() {
    const searchParams = useSearchParams()
    const name = searchParams.get('name')
    const age = searchParams.get('age')
    const gender = searchParams.get('gender')

    return (
        <div className="fn-app">
            <h1 className="fn-title">설문조사</h1>
            <h5 className="fn-sub-title">결과</h5>
            <div>
                <p className="fn-result-text">이름 : {name}</p>
                <p className="fn-result-text">나이 : {age}</p>
                <p className="fn-result-text">성별 : {gender === 'male' ? '남성' : '여성'}</p>
            </div>
        </div>
    )
}