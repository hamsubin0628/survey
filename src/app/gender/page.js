'use client'
import { useState } from "react";
import {useRouter, useSearchParams} from "next/navigation";

export default function PageGender() {
    const [gender, setGender] = useState('')
    const router = useRouter()
    const searchParams = useSearchParams()
    const name = searchParams.get('name')
    const age = searchParams.get('age')

    const handleSubmit = e => {
        e.preventDefault()
        router.push(`/result?name=${name}&age=${age}&gender=${gender}`)
    }

    return (
        <div className="fn-app">
            <h1 className="fn-title">설문조사</h1>
            <h5 className="fn-sub-title">성별</h5>
            <form onSubmit={handleSubmit}>
                <select className="fn-input" onChange={(e) => setGender(e.target.value)}>
                    <option value="">성별을 선택하세요</option>
                    <option value="male">남자</option>
                    <option value="female">여자</option>
                </select>
                <button className="fn-next-btn" type="submit">다음</button>
            </form>
        </div>
    )
}