'use client'
import { useState } from "react";
import {useRouter, useSearchParams} from "next/navigation";

export default function PageAge() {
    const [age, setAge] = useState('')
    const router = useRouter()
    const searchParams = useSearchParams()
    const name = searchParams.get('name')

    const handleSubmit = e => {
        e.preventDefault()
        router.push(`/gender?name=${name}&age=${age}`)
    }

    return (
        <div className="fn-app" onSubmit={handleSubmit}>
            <h1 className="fn-title">설문조사</h1>
            <h5 className="fn-sub-title">나이</h5>
            <form>
                <input className="fn-input" value={age} onChange={(e) => setAge(e.target.value)}/>
                <button className="fn-next-btn" type="submit">다음</button>
            </form>
        </div>
    )
}