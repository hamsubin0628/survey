'use client'
import { useState } from 'react'
import { useRouter } from 'next/navigation'

export default function Home() {
    const [name, setName] = useState('')
    const router = useRouter()

    const handleSubmit = e => {
        e.preventDefault()
        router.push(`/age?name=${name}`)
    }

    return (
        <div className="fn-app">
            <h1 className="fn-title">설문조사</h1>
            <h5 className="fn-sub-title">이름</h5>
            <form onSubmit={handleSubmit}>
                <input className="fn-input" value={name} onChange={(e) => setName(e.target.value)}/>
                <button className="fn-next-btn" type="submit">다음</button>
            </form>
        </div>
    );
}
